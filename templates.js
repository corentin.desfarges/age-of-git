module.exports = {
  building: {
    HOUSE: {
      motif: ' H ',
      width: 2,
      height: 2
    },
    FORGE: {
      motif: ' F ',
      width: 4,
      height: 3
    },
    BARRACK: {
      motif: ' B ',
      width: 5,
      height: 5
    },
    GATE_H: {
      model: ["[  ", "   ", "   ", "  ]"]
    },
    GATE_V: {
      model: ["MMM", "   ", "   ", "WWW"]
    }
  },
  wall: {
    HORIZONTAL: {
      SMALL: {
        motif: '---'
      },
      LARGE: {
        motif: '==='
      }
    },
    VERTICAL: {
      SMALL: {
        motif: ' | '
      },
      LARGE: {
        motif: '|||'
      }
    }
  }
};
