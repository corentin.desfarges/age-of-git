"use-strict";

const fs = require("fs");
const path = require("path");

const {name, map} = require("./configuration");
const templates = require("./templates");

const {width: mapWidth, height: mapHeight} = map;

const initMap = () => {
  const townMap = [];
  for (let i = 0; i < mapHeight; i++) {
    townMap[i] = new Array(mapWidth);
    for (let j = 0; j < mapWidth; j++) {
      townMap[i][j] = '   ';
    }
  }
  return townMap;
};

const displayTown = (townMapArray) => {
  if (name) {
    console.log(`Bienvenue à ${name} !`);
  }
  let str = '';
  for (let i = 0; i < townMapArray.length; i++) {
    for (let j = 0; j < townMapArray[i].length; j++) {
      str += townMapArray[i][j];
    }
    str += '\n';
  }
  console.log(str);
};

const checkAvailability = (position) => {
  return true;
};

const registerBuilding = (x, y, type, townMapArray) => {
  const {width, height, motif} = templates.building[type];
  for (let i = x ; i < x + width ; i++) {
    for (let j = y ; j < y + height ; j++) {
      if (checkAvailability(townMapArray[j][i])) {
        townMapArray[j][i] = motif;
      }
    }
  }
};

const registerWall = (orientation, x, y, width, type, townMapArray) => {
  const {motif} = templates.wall[orientation][type];

  if (orientation === "HORIZONTAL") {
    for (let i = x; i < width + x; i++) {
      if (checkAvailability(townMapArray[y][i])) {
        townMapArray[y][i] = motif;
      }
    }
  }

  if (orientation === "VERTICAL") {
    for (let i = y; i < width + y; i++) {
      if (checkAvailability(townMapArray[i][x])) {
        townMapArray[i][x] = motif;
      }
    }
  }
};

const registerCustomBuilding = (orientation, x, y, type, erase, townMapArray) => {
  const {model} = templates.building[type];
  for(let i = 0 ; i < model.length ; i++) {
    if (orientation === "HORIZONTAL") {

      if (erase || checkAvailability(townMapArray[y][i + x])) {
        townMapArray[y][i + x] = model[i];
      }
    }
    if (orientation === "VERTICAL") {

      if (erase || checkAvailability(townMapArray[i + x][y])) {
        townMapArray[i + x][y] = model[i];
      }
    }
  }
};

const townMap = initMap();

fs.readdirSync(path.resolve(__dirname, 'buildings'))
  .forEach((buildingFile) => {
    const {x, y, type} =  require(path.resolve(__dirname, 'buildings', buildingFile));
    registerBuilding(x, y, type, townMap)
  });

fs.readdirSync(path.resolve(__dirname, 'walls'))
  .forEach((wallFile) => {
    const {orientation, x, y, width, type} =  require(path.resolve(__dirname, 'walls', wallFile));
    registerWall(orientation, x, y, width, type, townMap);
  });

fs.readdirSync(path.resolve(__dirname, 'customBuildings'))
  .forEach((buildingFile) => {
    const {orientation, x, y, type, erase} =  require(path.resolve(__dirname, 'customBuildings', buildingFile));
    registerCustomBuilding(orientation, x, y, type, erase, townMap);
  });

displayTown(townMap);
